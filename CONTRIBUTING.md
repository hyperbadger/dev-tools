# Contributing

## Adding new binaries/scripts

1. Build a binary or add a script under `bin` directory and give a name that defines the purpose
1. Update `nfmp.yaml` in the root directory to include the new files; refer [nFPM config](https://nfpm.goreleaser.com/configuration) on how to handle configuration for packaging
1. Update the SemVer that makes sense in the `VERSION` file; packages will be published to [package registry](https://gitlab.com/hyperbadger/dev-tools/-/packages) based on this version
1. Create a tag once changes are merged to main branc; tag your repo with the same SemVar as updated in `VERSION` file

## Updating binaries/scripts

1. Update the required binary or script under `bin` directory
1. If required, update `nfmp.yaml` in the root directory to include new package configuration; refer [nFPM config](https://nfpm.goreleaser.com/configuration) on how to handle configuration for packaging
1. Update the SemVer that makes sense in the `VERSION` file; packages will be published to [package registry](https://gitlab.com/hyperbadger/dev-tools/-/packages) based on this version
1. Create a tag once changes are merged to main branch; tag your repo with the same SemVar as updated in `VERSION` file
