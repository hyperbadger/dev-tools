# dev-tools

dev-tools is used to build and package development tools available as binaries/scripts and publish to package registry

## Development binaries/scripts

Below binaries/scripts are currently packaged as `deb` packages and named as `dev-tools`

- **tf-init** - Wrapper around `terraform init` command to initialize terraform deployments; this gives some additional options to choose target environment
- **kustomize-render** - Wrapper around `kustomize build` command to perform some manipulation on manifest file

`dev-tools` package can be downloaded from [package registry](https://gitlab.com/hyperbadger/dev-tools/-/packages) which is version controlled based on SemVer defined in [VERSION](https://gitlab.com/hyperbadger/dev-tools/-/blob/main/VERSION) file

## Contributing

Refer to [CONTRIBUTING.md](https://gitlab.com/hyperbadger/dev-tools/-/blob/main/CONTRIBUTING.md) on how to add or update binaries/scripts to `dev-tools`
