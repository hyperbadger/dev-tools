#!/bin/bash
# shellcheck disable=SC1090,SC1091

set -euo pipefail

command -v kubectl >/dev/null 2>&1 || {
    echo -e "\033[0;31m kubectl needs to be installed to use kustomize-render. Aborting..!\033[m" >&2
    exit 1
}

cluster_dir="$1"
cluster=$(basename "${cluster_dir}")

if [[ $(kubectl config current-context) != *"${cluster}"* ]]; then
    echo "wrong cluster in current context"
    exit 1
fi

manifests_dir="manifests/${cluster}"
all_manifests_file="${manifests_dir}/all.yaml"
plant_config_file="${cluster_dir}/plant-configmap.yaml"

mkdir -p "${manifests_dir}"
rm -f "${manifests_dir}/*.yaml"
rm -f "${plant_config_file}"

seed=$(kubectl get configmap seed -n kube-system -o yaml)

for key_to_delete in .metadata.creationTimestamp .metadata.resourceVersion .metadata.uid; do
    seed=$(echo "${seed}" | yq -y "del(${key_to_delete})")
done

seed=$(echo "${seed}" | yq -y '.metadata.name="plant"')

namespaces=$(cd namespaces && ls -d -- *)

for namespace in $namespaces "kube-system"; do
    echo "---" >>"${plant_config_file}"
    echo "${seed}" | yq -y ".metadata.namespace=\"${namespace}\"" >>"${plant_config_file}"
done

kustomize build --enable-helm "${cluster_dir}" >"${all_manifests_file}"

# WARNING! The following was a last resort. Ideally these will become kustomize plugins.
# This is a hack to perform modifications using a sh script if it is not possible with kustomize.

test -f "${cluster_dir}/postrender.sh" && source "${cluster_dir}/postrender.sh" "${all_manifests_file}"
